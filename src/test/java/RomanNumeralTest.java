import org.junit.Test;
import static org.junit.Assert.*;

public class RomanNumeralTest {

    @Test
    public void testForI(){
        RomanNumeral rom = new RomanNumeral();
        assertEquals("I",rom.fromInt(1));
    }

    @Test
    public void testForV(){
        RomanNumeral rom = new RomanNumeral();
        assertEquals("V",rom.fromInt(5));
    }

    @Test
    public void testForX(){
        RomanNumeral rom = new RomanNumeral();
        assertEquals("X",rom.fromInt(10));
    }

    @Test
    public void testForL(){
        RomanNumeral rom = new RomanNumeral();
        assertEquals("L",rom.fromInt(50));
    }

    @Test
    public void testForC(){
        RomanNumeral rom = new RomanNumeral();
        assertEquals("C",rom.fromInt(100));
    }

    @Test
    public void testForD(){
        RomanNumeral rom = new RomanNumeral();
        assertEquals("D",rom.fromInt(500));
    }

    @Test
    public void testForM(){
        RomanNumeral rom = new RomanNumeral();
        assertEquals("M",rom.fromInt(1000));
    }

    @Test
    public void testForIV(){
        RomanNumeral rom = new RomanNumeral();
        assertEquals("IV",rom.fromInt(4));
    }

    @Test
    public void testForLI(){
        RomanNumeral rom = new RomanNumeral();
        assertEquals("LI",rom.fromInt(51));
    }

    @Test
    public void testForCMXCIX(){
        RomanNumeral rom = new RomanNumeral();
        assertEquals("CMXCIX",rom.fromInt(999));
    }

    @Test( expected = IllegalArgumentException.class)
    public void testForNegNum(){
        RomanNumeral rom = new RomanNumeral();
        rom.fromInt(-1);
    }



}
