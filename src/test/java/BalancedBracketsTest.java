

import org.junit.Test;
import static org.junit.Assert.*;

public class BalancedBracketsTest {

    //tests here
    @Test
    public void testOneForBalancedBrackets(){
        BalancedBrackets bal = new BalancedBrackets ();
        assertEquals(true,bal.hasBalancedBrackets("[LaunchCode]"));

    }

    @Test
    public void testTwoForBalancedBrackets(){
        BalancedBrackets bal = new BalancedBrackets ();
        assertEquals(true,bal.hasBalancedBrackets("Launch[Code]"));

    }

    @Test
    public void testThreeForBalancedBrackets(){
        BalancedBrackets bal = new BalancedBrackets ();
        assertEquals(true,bal.hasBalancedBrackets("[]LaunchCode"));

    }

    @Test
    public void testFourForBalancedBrackets(){
        BalancedBrackets bal = new BalancedBrackets ();
        assertEquals(true,bal.hasBalancedBrackets(""));

    }


    @Test
    public void testFiveForBalancedBrackets(){
        BalancedBrackets bal = new BalancedBrackets ();
        assertEquals(true,bal.hasBalancedBrackets("[][]"));

    }

    @Test
    public void testOneForUnbalancedBrackets(){
        BalancedBrackets bal = new BalancedBrackets ();
        assertEquals(false,bal.hasBalancedBrackets("[LaunchCode"));

    }

    @Test
    public void testTwoForUnbalancedBrackets(){
        BalancedBrackets bal = new BalancedBrackets ();
        assertEquals(false,bal.hasBalancedBrackets("Launch]Code["));

    }

    @Test
    public void testThreeForUnbalancedBrackets(){
        BalancedBrackets bal = new BalancedBrackets ();
        assertEquals(false,bal.hasBalancedBrackets("["));

    }

    @Test
    public void testFourForUnbalancedBrackets(){
        BalancedBrackets bal = new BalancedBrackets ();
        assertEquals(false,bal.hasBalancedBrackets("[[]"));

    }

    @Test
    public void testFiveForUnbalancedBrackets(){
        BalancedBrackets bal = new BalancedBrackets ();
        assertEquals(false,bal.hasBalancedBrackets("[]["));

    }

}
