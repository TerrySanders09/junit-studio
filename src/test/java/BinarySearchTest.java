import org.junit.Test;
import static org.junit.Assert.*;

public class BinarySearchTest {

    //tests here
    @Test
    public void testOneForBinarySearch(){
        BinarySearch bs = new BinarySearch ();
        int[] myIntArray = {1,2,3,4,5};
        assertEquals(0,bs.binarySearch(myIntArray,1));
    }

    @Test
    public void testTwoForBinarySearch(){
        BinarySearch bs = new BinarySearch ();
        int[] myIntArray = {1,2,3,4,5};
        assertEquals(-1,bs.binarySearch(myIntArray,9));
    }

    @Test
    public void testThreeForBinarySearch(){
        BinarySearch bs = new BinarySearch ();
        int[] myIntArray = {1,2,3,4,5};
        assertEquals(2,bs.binarySearch(myIntArray,3));
    }

    @Test
    public void testFourForBinarySearch(){
        BinarySearch bs = new BinarySearch ();
        int[] myIntArray = {1,2,3,4,5};
        assertEquals(4,bs.binarySearch(myIntArray,5));
    }


}
